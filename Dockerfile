From bitnami-docker-wordpress:latest

# Required by W3 Total Cache: https://github.com/bitnami/bitnami-docker-wordpress/issues/130
sed -i -e 's/#LoadModule expires_module/LoadModule expires_module/g' /opt/bitnami/apache/conf/httpd.conf
sed -i -e 's/#LoadModule ext_filter_module/LoadModule ext_filter_module/g' /opt/bitnami/apache/conf/httpd.conf
